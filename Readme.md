# NPM Image resize script

## Usage

1. install node.js from https://nodejs.org/en/
2. clone NPM image resizer `git clone git@gitlab.com:joernba/npm-image-resize.git`
3. install modules: `npm install`
4. replace all paths inside package.json scripts (i.e. immagini/Cantieri/small) with the ones suitable for your project
5. run resize script: `npm run build:images`
